# Dunedain
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Dunedain nothing to be said yet.

## Installation

Dunedain uses Meson as its build system. It generates ninja files that only needs to be configured once.

```bash
$ mkdir -p build
$ cd build
$ meson ..

```
To build the project, in the build directory

```bash
$ ninja
$ ./dunedain

```

## Contributing
See CONTRIBUTING for more information

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
See LICENSE for more information
[MIT](https://choosealicense.com/licenses/mit/)
